package testes;

import AG.Config;

public class main {

	public static void main(String[] args) {
		
		double[] bonus = {0.0, 0.5, 1.0};

		int NtestePar = 1;
		int Nteste10 = (int) (Config.TAM_POP * 0.10);
		int Nteste50 = (int) (Config.TAM_POP * 0.50);
		
		int[] nTests = {NtestePar, Nteste10, Nteste50};		

		CasoTeste caso1;
		
		for (double peso : bonus) {
			Config.PESO_DO_BONUS  = peso;
			caso1 = new CasoTeste();			
			caso1.executar();			
			System.out.println(caso1);			
		}
		
		CasoTeste caso2x;
		Config.PESO_DO_BONUS  = 0.4;
		
		for (int nTest : nTests) {
			Config.SAMPLE_COMPARATOR_SIZE  = nTest;
			caso2x = new CasoTeste();			
			caso2x.executar();			
			System.out.println(caso2x);			
		}		
	}
}
