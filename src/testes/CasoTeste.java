package testes;

import java.util.Arrays;
import java.util.*;

import AG.Config;

public class CasoTeste {
	
	Execucao[] execucoes = new Execucao[10];	
	List<List<int[]>> listaDeListas = new ArrayList<List<int[]>>();  
	
	
	public void executar(){
		
		Config.FITNESS_TABLE =  Config.INDIVIDUAL_FITNESS_TABLE;  
		
		for (int i = 0; i < 5; i++) {
			execucoes[i] = new Execucao();
			execucoes[i].executar();
		}
		Config.FITNESS_TABLE =  Config.GROUP_FITNESS_TABLE;
		for (int i = 5; i < 10; i++) {
			execucoes[i] = new Execucao();
			execucoes[i].executar();
		}
		
		for (int i = 0; i < execucoes.length; i++) {
			listaDeListas.add(execucoes[i].getGraf());
		}
		csv.gerar(listaDeListas, this.toString());

		
	}
	
	


	@Override
	public String toString() {
		String retorno = "CasoTeste \n";
		retorno += "\n Bonus:"+Config.PESO_DO_BONUS+" \n";
		retorno += "\n n_teste:"+Config.SAMPLE_COMPARATOR_SIZE+" \n";
		retorno += "\n Avalia��o indicidual\n";
		for (int i = 0; i < 5; i++) {
			retorno += execucoes[i];
			
		}
		
		retorno += "Popula��o Inicial:\n"+execucoes[0].getPopulacaoInicial();
		retorno += "Popula��o Final:\n"+execucoes[0].getPopulacaoFinal();
				
		retorno += "\n Avalia��o em rela��o ao grupo\n";
		for (int i = 5; i < 10; i++) {
			retorno += execucoes[i];
		}
		
		retorno += "Popula��o Inicial:\n"+execucoes[5].getPopulacaoInicial();
		retorno += "Popula��o Final:\n"+execucoes[5].getPopulacaoFinal();
		
		return retorno;
	}




	public Execucao[] getExecucoes() {
		return execucoes;
	}


	public void setExecucoes(Execucao[] execucoes) {
		this.execucoes = execucoes;
	}
	
	

}
