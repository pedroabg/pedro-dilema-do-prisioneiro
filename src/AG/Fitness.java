package AG;
public class Fitness {

	// pena em meses 0 , 6 , 360, 120

	public static final int PENA_O_OUTRO_COOPERA = 0;
	public static final int PENA_DOIS_COOPERAM = 6;
	public static final int PENA_VOCE_COOPERA = 360;
	public static final int PENA_DOIS_ENTREGAM = 120;


	
	
	public static double calculateFitness(Populacao sample, Individuo you){
		double partialFitness = 0;
		
		   for (Individuo other : sample.getIndividuos()) {
			   partialFitness += compare(you,other);
		   } 		   
		   //System.out.println(partialFitness/sample.getIndividuos().size());
		   
		   return partialFitness/sample.getIndividuos().size();
		
	}



	private static double compare(Individuo you, Individuo other) {
//		int gene1, gene2;
		double[][] table = Config.FITNESS_TABLE;
		int[] chain1 = you.getCadeiaDecodificada();
		int[] chain2 = other.getCadeiaDecodificada();
		double partialFitness = 0;
		
		for (int i = 0; i < chain1.length; i++) {
			partialFitness += table[chain1[i]][chain2[i]];
			//System.out.println(table[chain1[i]][chain2[i]]);
		}
		
//		System.out.println("Media: "+partialFitness/chain1.length);
		
		return partialFitness/chain1.length;
//		return partialFitness;
	}



	public static int calculaBonus(Individuo you) {
		int bonus = 0;
		int seqCoop = 0;
		
		int [] cadeia =  you.getGenoma();
		
		for (int i = 0; i < cadeia.length; i++) {
			if(cadeia[i] < 5){				
				seqCoop++;
				if(seqCoop >= Config.BONUS_SEQ){
					bonus += bonusParcial(i,cadeia);
					seqCoop = 0;
				}
			} else{
				seqCoop = 0;
			}
		}
		
//		double bonusMax = (cadeia.length - Config.BONUS_SEQ)*9;		
//		bonus = (int)(((double)bonus*100)/bonusMax);
		bonus = (int)(((double)bonus*Config.PESO_DO_BONUS));
		
//		System.out.println("bonus "+bonus);
		return bonus;

	}



	private static int bonusParcial(int i, int[] cadeia) {
		int bonus = 0;
		int max = Config.BONUS_SEQ * 9;
		for (int j = 0; j < Config.BONUS_SEQ; j++) {
			bonus += cadeia[i - j];
		}
		bonus = max - bonus;
		return bonus/Config.BONUS_SEQ;
	}

}
