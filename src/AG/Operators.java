package AG;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Operators {
	
	static MersenneTwisterFast prng = new MersenneTwisterFast();

	public static Populacao sampleTorneio(Populacao populacao) {
		List<Individuo> individuos = new ArrayList<Individuo>(
				populacao.getIndividuos());
		Populacao parents;

		int totalTarget = Config.LAMBIDA;
		List<Individuo> tempSublist = new ArrayList<Individuo>();
		List<Individuo> selected = new ArrayList<Individuo>();
		Individuo selectedInd;

		// System.out.println("1 "+individuos);

		while (selected.size() < totalTarget) {
			Collections.shuffle(individuos);
			//System.out.println("Embaralhados: \n"+individuos);
			// Como o tamanho do ring ser� empre menor que o total de
			// individuos, e a sublista avan�a
			// de 1 em 1, n�o corre o risco de haver uma sele��o out of bounds.
			tempSublist = new ArrayList<Individuo>(individuos.subList(0,
					Config.RING));
			// System.out.println("Selecionados para o round: \n"+tempSublist);
			Collections.sort(tempSublist);
			// System.out.println("Lista ordenada \n"+tempSublist);
			selectedInd = tempSublist.remove(0);
			selected.add(selectedInd);
			// System.out.println("Individuo selecionado \n"+selectedInd);
			// System.out.println("antes de remover\n"+individuos);
			individuos.remove(selectedInd);
			// System.out.println(individuos);
			// System.out.println("Quantos j� foram selecionados: "+selected.size());

		}

		// System.out.println("Pais selecionados: \n"+selected);

		parents = new Populacao();
		parents.setIndividuos(selected);

		return parents;
	}

	public static Populacao cruzar(Populacao popCruzar) {
		//System.out.println("Popula��o que vai cruzar: \n"+popCruzar);
		
		List<Individuo> individuos = new ArrayList<Individuo>(
				popCruzar.getIndividuos());
		List<Individuo> listafinal = new ArrayList<Individuo>();
		Individuo[] filhos = new Individuo[2];
		Populacao popPosCruzamento = new Populacao();

		// System.out.println(individuos.size());
		// System.out.println("individuos na ordem de cruzamento:\n"+individuos);

		if (individuos.size() % 2 != 0)
			listafinal.add(individuos.remove(0));

		//System.out.println(individuos.size());

		for (int i = 0; i < individuos.size(); i++) {
			if(podeCruzar()){
				//System.out.println("v�o cruzar: " + individuos.get(i)+" com " + individuos.get(i + 1));
				filhos = cruzar(individuos.get(i), individuos.get(i + 1));		
				listafinal.add(filhos[0]);
				listafinal.add(filhos[1]);
			}else{
				listafinal.add(individuos.get(i));
				listafinal.add(individuos.get(i+1));
			}
			i++;
		}
		
		popPosCruzamento.setIndividuos(listafinal);
		
		//System.out.println("Lista Final: \n"+popPosCruzamento);

		// List<Integer> listOfRam

		return popPosCruzamento;
	}

	private static Individuo[] cruzar(Individuo individuo1, Individuo individuo2) {
		List<Integer> listOfRandomIndexes = new ArrayList<Integer>(getRandomIndexes(individuo1));
		//System.out.println("Lista de indices que v�o cruzar "+listOfRandomIndexes);
		Individuo[] filhos = {individuo1,individuo2};
		int genIndex, mediana;
		int[] cadeia1 = individuo1.getGenoma();
		int[] cadeia2 = individuo2.getGenoma();
	
		//System.out.println("cruzou!");
		for (int i = 0; i < listOfRandomIndexes.size(); i++) {
			genIndex = listOfRandomIndexes.get(i);
			mediana = mediana(cadeia1[genIndex], cadeia2[genIndex]);
			cadeia1[genIndex] = mediana;
			cadeia2[genIndex] = mediana;			
			
		}
		individuo1.setGenoma(cadeia1);
		individuo2.setGenoma(cadeia2);
		filhos[0] = individuo1;
		filhos[1] = individuo2;		
		
		return filhos;
	}

	private static int mediana(int i, int j) {
		int m;
		int a = 0;
		//se for impar, sorteia se vai somar 1 ou 0, se somar a media vai dar b, caso contrario a		
		if((i+j)%2 == 1){
			a += prng.nextInt(2);
//			System.out.println(a);
		}
		
		m = (i+j+a)/2;
		
		//System.out.println("Mediana: "+m);
		
		return m;
	}

	private static boolean podeCruzar() {
		int dado = prng.nextInt(100);
		//System.out.println("dado: "+dado);
		return  dado < Config.PCruzamento*100;
	}

	private static List<Integer> getRandomIndexes(Individuo individuo) {
		List<Integer> listOfRandomIndexes = new ArrayList<Integer>();
		// gerar uma lista com valores de 0 a n , n � o tamanho da cadeia
		for (int i = 0; i < individuo.getCadeiaDecodificada().length; i++) {
			listOfRandomIndexes.add(i);
		}
		Collections.shuffle(listOfRandomIndexes);
		return listOfRandomIndexes.subList(0, Config.PGC);
	}

}
