package AG;

public class Solucao {
	
	
	String alvo;
	int fitnessAdequado;
	boolean solucaoAceita;
	
	
	
	public Solucao(String alvo) {
		super();
		this.alvo = alvo;
		solucaoAceita = false;
	}

	public String getAlvo() {
		return alvo;
	}

	public void setAlvo(String alvo) {
		this.alvo = alvo;
	}
	
	
	public boolean isSolucaoAceita(Populacao populacao){
		
		return solucaoAceita;
	}

	public int getFitnessAdequado() {
		return fitnessAdequado;
	}

	public void setFitnessAdequado(int fitnessAdequado) {
		this.fitnessAdequado = fitnessAdequado;
	}

	public boolean isSolucaoAceita() {
		return solucaoAceita;
	}

	public void setSolucaoAceita(boolean solucaoAceita) {
		this.solucaoAceita = solucaoAceita;
	}
	
	

	
	
	
	

}
