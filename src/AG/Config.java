package AG;

public class Config {
	
	public static int TAM_POP = 40;
	public static int NUM_GENES = 30;
	
	
	public static final int BOTH_BAD_IND_FITNESS = 66;
	public static final int ONLY_U_BAD_IND_FITNESS = 100;
	public static final int ONLY_U_GOD_IND_FITNESS = 0;
	public static final int BOTH_GOD_IND_FITNESS = 98;

	public static double[][] INDIVIDUAL_FITNESS_TABLE = {
			{ BOTH_BAD_IND_FITNESS, ONLY_U_BAD_IND_FITNESS },
			{ ONLY_U_GOD_IND_FITNESS, BOTH_GOD_IND_FITNESS } };
	
	
	//Tabela do fitness de grupo
	public static final int BOTH_BAD_GROUP_FITNESS = 0;
	public static final int ONLY_U_BAD_GROUP_FITNESS = 40;
	public static final int ONLY_U_GOD_GROUP_FITNESS = 40;
	public static final int BOTH_GOD_GROUP_FITNESS = 100;

	public static double[][] GROUP_FITNESS_TABLE = {
			{ BOTH_BAD_GROUP_FITNESS, ONLY_U_BAD_GROUP_FITNESS },
			{ ONLY_U_GOD_GROUP_FITNESS, BOTH_GOD_GROUP_FITNESS } };
	
	
	//Qual porcentagem da populacao o ind�viduo ser� comparado para gerar o fitness; setar 10% ou 50%
	public static double SAMPLE_COMPARATOR_FACTOR = 0.10;
	
	// Ou setar 1 para par-a par
	public static int SAMPLE_COMPARATOR_SIZE = (int) (TAM_POP * SAMPLE_COMPARATOR_FACTOR);
//	public static int SAMPLE_COMPARATOR_SIZE = 1;
	
	// Qual tabela de fitness ir� utilizar, INDIVIDUAL_FITNESS_TABLE ou GROUP_FITNESS_TABLE
	public static double[][] FITNESS_TABLE =  INDIVIDUAL_FITNESS_TABLE;  
	
	//Porcentagem de pais selecionados para cruzamento; setar de 5% a 20%
	public static double LAMBIDA_fator = 0.15;
	public static int LAMBIDA = (int) (TAM_POP * LAMBIDA_fator);
	
	//Quantidade de participantes do torneio. Deve ser menor que lambida
	public static int RING = 2;
	
	//Porcentagem de genes que ir�o cruzar; setar de 5% a 20%
	public static double PGC_fator = 0.15;
	public static int PGC = (int) (NUM_GENES * PGC_fator);
	
	public static double PM = 0.2;
	
	public static double PCruzamento = 0.8;
	
	
	//Tamanho da cadeia de bonus
	public static int BONUS_SEQ = 3;
	public static double PESO_DO_BONUS = 1.0;
	
	//quantas gera��es o melhor fitness deve permanecer inalteradom para o algoritimo parar	
	public static int K = 10;

	public static int MAX_GERACOES = 500;
	
	
	
	

}
















