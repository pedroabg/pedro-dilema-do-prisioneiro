package AG;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Individuo implements Comparable<Individuo> {

	int fitness;
	int[] genoma;
	// 1 coopera 0 n�o coopera
	int[] cadeiaDecodificada;
	int geracao;
	Populacao populacao;

	static MersenneTwisterFast prng = new MersenneTwisterFast();

	double probMutacaco;

	public int getFitness() {
		return fitness;
	}

	public void setFitness(int fitness) {
		this.fitness = fitness;
	}

	public int[] getGenoma() {
		return genoma;
	}

	public void setGenoma(int[] genoma) {
		this.genoma = genoma;
	}

	public Individuo() {
		super();
		fitness = -1;
		probMutacaco = 0.4;

	}

	static Individuo GerarIndividuo(int numGenes, int geracao) {

		Individuo individuo = new Individuo();
		individuo.genoma = new int[numGenes];
		individuo.setGeracao(geracao);

		for (int i = 0; i < numGenes; i++) {

			individuo.genoma[i] = prng.nextInt(10);
		}

		individuo.decodificaCadeia();
		// individuo.populacao = populacao;

		return individuo;
	}

	public void decodificaCadeia() {
		int[] cadeiaDecodificada = new int[this.genoma.length];

		for (int i = 0; i < this.genoma.length; i++) {
			cadeiaDecodificada[i] = decodificaGene(this.genoma[i]);

		}

		this.cadeiaDecodificada = cadeiaDecodificada;

	}

	private int decodificaGene(int gene) {
		return gene < 5 ? 1 : 0;
	}

	public int qtdCooperacao() {
		int qtd = 0;
		for (int i = 0; i < cadeiaDecodificada.length; i++) {
			qtd += cadeiaDecodificada[i];
		}

		return qtd;
	}
	
	public int qtdDelacao() {
		int qtd = 0;
		qtd += (cadeiaDecodificada.length - qtdCooperacao());
		
		return qtd;
	}

	public int compareTo(Individuo o) {
		// TODO Auto-generated method stub

		return o.getFitness() - this.fitness;
	}

	public int getGeracao() {
		return geracao;
	}

	public void setGeracao(int geracao) {
		this.geracao = geracao;
	}

	public int[] getCadeiaDecodificada() {
		return cadeiaDecodificada;
	}

	public void setCadeiaDecodificada(int[] cadeiaDecodificada) {
		this.cadeiaDecodificada = cadeiaDecodificada;
	}

	public void avaliar(Populacao populacaoTotal) {
		int finalFitness = 0;
		Individuo you = this;
		// System.out.println(populacaoTotal);
		Populacao popComparada = Populacao.getSample(populacaoTotal,
				Config.SAMPLE_COMPARATOR_SIZE);
		// System.out.println(populacaoTotal);
		finalFitness = ((int) Fitness.calculateFitness(popComparada, you));
		// System.out.println("Fitness da compara��o: "+finalFitness);
		finalFitness += Fitness.calculaBonus(you);
		// System.out.println("Bonus: "+Fitness.calculaBonus(you));
		this.fitness = finalFitness;
		// System.out.println("comparar "+this+" com: "+popComparada);
	}

	public int checaFitness(Individuo parceiro) {
		int fitness = 0;
		int gene1, gene2;

		return fitness;
	}

	public void checaMutacao() {
		int dado;
		int[] cadeia = this.genoma;

		for (int i = 0; i < cadeia.length; i++) {
			if (isMutavel()) {
				// System.out.println("muta��o");
				cadeia[i] = mutacaoCreep(cadeia[i]);

			}
		}

		this.genoma = cadeia;

	}

	private boolean isMutavel() {
		// Numero sorteado entre 0 e 100,
		// se ele for menor que a probabilidade significa que
		// que o sorteio caiu dentro da probabilidade
		int dado = prng.nextInt(100);
		// System.out.println(dado);
		// System.out.println((Config.PM * 100));
		return dado <= (Config.PM * 100);
	}

	private int mutacaoCreep(int gene) {
		int[] painelDeEventos = new int[100];
		int dado = prng.nextInt(100);

		for (int i = 0; i < 60; i++) {
			painelDeEventos[i] = gene + 1;
			i++;
			painelDeEventos[i] = gene - 1;
		}
		for (int i = 60; i < 90; i++) {
			painelDeEventos[i] = gene + 2;
			i++;
			painelDeEventos[i] = gene - 2;
		}
		for (int i = 90; i < 100; i++) {
			painelDeEventos[i] = gene + 3;
			i++;
			painelDeEventos[i] = gene - 3;
		}

		// System.out.print("\n Gene que ir� mudar: " + gene +
		// "; Dado sorteado "
		// + dado + " ; novo gene: " + painelDeEventos[dado] + "\n");

		// painelDeEventos[dado]
		if (painelDeEventos[dado] < 0)
			painelDeEventos[dado] += 9;
		if (painelDeEventos[dado] > 9)
			painelDeEventos[dado] -= 9;

		// System.out.print("\n Gene corrigido: " + painelDeEventos[dado] +
		// "\n");

		return painelDeEventos[dado];
	}

	public Individuo(Individuo other) {
		super();
		this.fitness = other.getFitness();
		this.genoma = other.getGenoma().clone();
		this.cadeiaDecodificada = other.getCadeiaDecodificada().clone();
		this.geracao = other.getGeracao();
	}

	@Override
	public String toString() {
		return "" + Arrays.toString(genoma) + " , fitness = "
				+ fitness + "\n";
		// + Arrays.toString(cadeiaDecodificada)+ "\n";
	}
}
